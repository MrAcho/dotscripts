#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
alias ls='ls --color=auto'
cd(){

	builtin cd $@
	ls
}

PS1='[\u@\h \W]\$ '


greeter=('neofetch' 'cirnosay' 'cowsay' 'ponysay' 'botsay' 'catsay' )
wordslist=(
'debelak'
'izlezni navunka malko ve'
'Putkata purdi li???'
'are we zapek'
'PHHAHHAhAHAH polzvash linux'
"EBATI TUPQ MANGAL SI ROSENE"
'STROIM LI ROSENE?'
'ARE VMESTO GO OTVORISH TOQ TERMINAL DA OTVORISH MALKO PUTKI!!!'
'praskai se pedal'
'duhai baluk'
'vzemi se v ruce malko shefe'
'8========> WOOWOWOOWWW!!!'
)

exec ${greeter[$(( $RANDOM % ${#greeter[@]} ))]} ${wordslist[$(( $RANDOM % ${#wordslist[@]} ))]}

